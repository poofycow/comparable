package com.poofycow.utils.comparator;

import java.util.LinkedList;

public class ComparableLinkedList<T> extends LinkedList<T> implements Comparable {
    private static final long serialVersionUID = -3537520532757068966L;

    @Override
    public boolean compare(Comparable b) {
        try {
            return Comparator.compare(this, b);
        } catch (IllegalAccessException e) {
            e.printStackTrace();
            return false;
        }
    }
}
